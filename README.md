# SharpSpring Webforms

## INTRODUCTION

The Sharpspring Webforms module extends the SharpSpring module's
(https://www.drupal.org/project/sharpspring)
functionality to add SharpSpring lead tracking to Webforms.

For a full description of the module, visit the project page:
<https://www.drupal.org/project/sharpspring_webforms>

To submit bug reports and feature suggestions, or to track changes:
<https://www.drupal.org/project/issues/2480987>

## REQUIREMENTS

This module requires the following modules:
* [SharpSpring](https://www.drupal.org/project/sharpspring)
* [Webform](https://www.drupal.org/project/webform)

## INSTALLATION

Install as usual, see <http://drupal.org/node/895232> for further information.

## CONFIGURATION

This module contains 2 ways of configuring the connection with SharpSpring.
Both ways are not compatible and should not be used together as this may result
in duplicate submissions.

### Native Form Embed Code

This way will adds a javascript to your webform which will submit all values
when your form is submitted.

Configure user permissions in Administration » People » Permissions:

* configure webform sharpspring<br>Users with this permission will be
able to configure the sharpspring webforms module settings.

Enable SharpSpring lead tracking in Administration » Structure » Webforms »
{{ webform }} » SharpSpring or on URL
**/admin/structure/webform/manage/{webform}/sharpspring**.

You can enable or disable for each individual webform and configure
the base URI and endpoint.

### Webform Handler

When for some reason the default SharpSpring javascript is not sufficient,
e.g. when working with a complex form structure like multi step forms
you can use the Webform Handler. This Handler will send all submission data once
your form submissions is succesfully completed.

More information can be found [here](https://help.sharpspring.com/hc/en-us/articles/360022123931-Integrating-SharpSpring-with-Contact-Form-7-Using-PHP#h_744751456801548155505810).
