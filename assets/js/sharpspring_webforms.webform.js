(function (Drupal, drupalSettings, window) {
  'use strict';

  var __ss_noform = __ss_noform || [];
  for (var form_id in drupalSettings.sharpspring_webforms) {
    var sharpspring_webform_settings = drupalSettings.sharpspring_webforms[form_id];
    __ss_noform.push(['baseURI', sharpspring_webform_settings.base_uri]);
    __ss_noform.push(['form', sharpspring_webform_settings.form_id, sharpspring_webform_settings.endpoint]);
  }
  window.__ss_noform = __ss_noform;

  var sharpspring = drupalSettings.sharpspring || false;

  if (sharpspring) {
    var ss = document.createElement('script');
    ss.type = 'text/javascript';
    ss.async = true;
    ss.src = (document.location.protocol === 'https:' ? 'https://' : 'http://') + sharpspring.domain + '/client/noform.js';
    document.body.appendChild(ss);
  }

})(Drupal, drupalSettings, window);
