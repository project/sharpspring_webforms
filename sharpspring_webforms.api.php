<?php

/**
 * @file
 * Hooks provided by the sharpspring Webforms module.
 */

use Drupal\webform\WebformSubmissionInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter data send to SharpSpring using the webform plugin.
 */
function hook_sharpspring_webform_handler_data_alter(array &$data, WebformSubmissionInterface $webform_submission) {
  $data['created'] = $webform_submission->getCreatedTime();
  unset($data['my_custom_data']);
}

/**
 * @} End of "addtogroup hooks".
 */
