<?php

namespace Drupal\sharpspring_webforms\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform\WebformInterface;

/**
 * Custom access control handler for the webform SharpSpring settings.
 */
class WebformSharpspringAccess {

  /**
   * Check that webform SharpSpring config can be updated by a user.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   A webform.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function checkWebformSharpspringAccess(WebformInterface $webform, AccountInterface $account) {
    return $webform->access('update', $account, TRUE)
      ->andIf(AccessResult::allowedIfHasPermission($account, 'configure webform sharpspring'));
  }

}
