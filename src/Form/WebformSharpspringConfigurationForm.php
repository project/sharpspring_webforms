<?php

namespace Drupal\sharpspring_webforms\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form that can be used to configure the sharpspring webform settings.
 */
class WebformSharpspringConfigurationForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();

    $form['sharpspring_configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('SharpSpring'),
    ];
    $form['sharpspring_configuration']['sharpspring_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate sharpspring tracking'),
      '#description' => $this->t('Submission data will be send to SharpSpring if activated.'),
      '#default_value' => $webform->getThirdPartySetting('sharpspring_webforms', 'status'),
    ];
    $form['sharpspring_configuration']['sharpspring_base_uri'] = [
      '#type' => 'url',
      '#title' => $this->t('Base URI'),
      '#default_value' => $webform->getThirdPartySetting('sharpspring_webforms', 'base_uri'),
      '#states' => [
        'required' => [
          ':input[name="sharpspring_status"]' => ['checked' => TRUE],
        ],
        'visible' => [
          ':input[name="sharpspring_status"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['sharpspring_configuration']['sharpspring_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#default_value' => $webform->getThirdPartySetting('sharpspring_webforms', 'endpoint'),
      '#states' => [
        'required' => [
          ':input[name="sharpspring_status"]' => ['checked' => TRUE],
        ],
        'visible' => [
          ':input[name="sharpspring_status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ((bool) $form_state->getValue('sharpspring_status') === TRUE && $form_state->isValueEmpty('sharpspring_base_uri')) {
      $form_state->setErrorByName('sharpspring_base_uri', $this->t('@name field is required.', ['@name' => $form['sharpspring_configuration']['sharpspring_base_uri']['#title']]));
    }

    if ((bool) $form_state->getValue('sharpspring_status') === TRUE && $form_state->isValueEmpty('sharpspring_endpoint')) {
      $form_state->setErrorByName('sharpspring_endpoint', $this->t('@name field is required.', ['@name' => $form['sharpspring_configuration']['sharpspring_endpoint']['#title']]));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    if ((bool) $form_state->getValue('sharpspring_status') === TRUE) {
      $webform->setThirdPartySetting(
        'sharpspring_webforms',
        'status',
        (bool) $form_state->getValue('sharpspring_status')
      );
      $sharpspring_base_uri = $form_state->getValue('sharpspring_base_uri');
      $webform->setThirdPartySetting(
        'sharpspring_webforms',
        'base_uri',
        // Make sure the base_uri is saved with a trailing slash or tracking
        // won't work.
        rtrim(is_string($sharpspring_base_uri) ? $sharpspring_base_uri : '', '/') . '/'
      );
      $webform->setThirdPartySetting(
        'sharpspring_webforms',
        'endpoint',
        $form_state->getValue('sharpspring_endpoint')
      );
    }
    else {
      $webform->unsetThirdPartySetting('sharpspring_webforms', 'status');
      $webform->unsetThirdPartySetting('sharpspring_webforms', 'base_uri');
      $webform->unsetThirdPartySetting('sharpspring_webforms', 'endpoint');
    }

    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
    return parent::save($form, $form_state);
  }

}
