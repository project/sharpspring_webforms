<?php

namespace Drupal\sharpspring_webforms\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Webform submission remote post handler.
 *
 * @WebformHandler(
 *   id = "sharpspring_remote_post",
 *   label = @Translation("SharpSpring Remote Post"),
 *   category = @Translation("External"),
 *   description = @Translation("Posts webform submissions to SharpSpring."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = FALSE,
 * )
 */
class SharpSpringWebformHandler extends WebformHandlerBase {

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * The HTTP Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('http_client');
    $instance->currentRequest = $container->get('request_stack')->getCurrentRequest();
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'base_uri' => '',
      'endpoint' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#theme' => 'webform_handler_settings_summary',
      '#settings' => [
        'settings' =>
          [
            [
              'title' => $this->t('Base URI'),
              'value' => $this->configuration['base_uri'],
            ],
            [
              'title' => $this->t('Endpoint'),
              'value' => $this->configuration['endpoint'],
            ],
          ],
      ],
      '#handler' => $this,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['sharpspring'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('SharpSpring configuration'),
      '#tree' => FALSE,
    ];
    $form['sharpspring']['base_uri'] = [
      '#type' => 'url',
      '#title' => $this->t('Base URI'),
      '#default_value' => $this->configuration['base_uri'] ?? NULL,
      '#required' => TRUE,
    ];
    $form['sharpspring']['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#default_value' => $this->configuration['endpoint'] ?? NULL,
      '#required' => TRUE,
    ];

    $this->elementTokenValidate($form);

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE): void {
    // Only post a form to SharpSpring when it is completed and inserted.
    if ($update === FALSE && $webform_submission->isCompleted()) {
      $this->remotePost($webform_submission);
    }
  }

  /**
   * Do the actual post to SharpSpring.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission.
   */
  protected function remotePost(WebformSubmissionInterface $webform_submission): void {
    $data = $this->buildPostData($webform_submission);

    try {
      $this->doRemotePost($data);
    }
    catch (GuzzleException $e) {
      $context = [
        'link' => $webform_submission->toLink($this->t('Edit'), 'edit-form')
          ->toString(),
        'webform_submission' => $webform_submission,
        'handler_id' => $this->getHandlerId(),
        '%reason' => $e->getMessage(),
      ];
      $this->getLogger('webform_submission')->error('Call to SharpSpring failed. Reason: %reason', $context);
    }
  }

  /**
   * The actual call to SharpSpring.
   *
   * @param array $data
   *   The data that should be posted to SharpSpring.
   */
  protected function doRemotePost(array $data): void {
    $this->httpClient->request('get', $this->buildUrl(), ['query' => $data]);
  }

  /**
   * Helper method that flattens the data that should be sent to SharpSpring.
   *
   * @param array $array
   *   The array that should be flattened.
   * @param string $prefix
   *   The prefix.
   *
   * @return array
   *   A flattened array.
   */
  protected function flattenData(array $array, string $prefix = ''): array {
    $result = [];
    foreach ($array as $key => $value) {
      if (is_array($value)) {
        $result += $this->flattenData($value, $prefix . $key . '__');
      }
      else {
        $result[$prefix . '__' . $key] = $value;
      }
    }
    return $result;
  }

  /**
   * Build the SharpSpring base URL.
   *
   * @return string
   *   The URL.
   */
  protected function buildUrl() {
    return rtrim($this->configuration['base_uri'], '/') . '/' . $this->configuration['endpoint'] . '/jsonp/';
  }

  /**
   * Helper method that builds the data that should be sent to SharpSpring.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission entity.
   *
   * @return array
   *   The post data.
   */
  protected function buildPostData(WebformSubmissionInterface $webform_submission) {
    $data = [];
    $elements = $webform_submission->getWebform()->getElementsInitializedFlattenedAndHasValue();

    foreach ($elements as $key => $element) {
      $value = $webform_submission->getElementData($key);

      if (is_array($value)) {
        if ($element['#webform_composite'] === TRUE) {
          $data = array_merge($data, $this->flattenData($value, $key));
        }
        else {
          $data[$key] = implode(',', $value);
        }
      }
      else {
        $data[$key] = $value;
      }
    }

    // Add the SharpSpring tracking id when present.
    if ($this->currentRequest instanceof Request && $this->currentRequest->cookies->has('__ss_tk') !== NULL) {
      $data['trackingid__sb'] = $this->currentRequest->cookies->get('__ss_tk');
    }

    $this->moduleHandler->alter('sharpspring_webform_handler_data', $data, $webform_submission);

    return $data;
  }

}
