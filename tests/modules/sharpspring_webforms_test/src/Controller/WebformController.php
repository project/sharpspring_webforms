<?php

namespace Drupal\sharpspring_webforms_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller that contains sharpspring webform related code for tests.
 */
class WebformController extends ControllerBase {

  /**
   * The webform storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $webformStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->webformStorage = $entity_type_manager->getStorage('webform');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * List all existing webforms.
   *
   * @return array
   *   A renderable array containing all webforms.
   */
  public function listAll(): array {
    /** @var \Drupal\webform\WebformInterface[] $webforms */
    $webforms = $this->webformStorage->loadMultiple();

    $build = [];

    foreach ($webforms as $webform) {
      $build[] = $webform->getSubmissionForm();
    }

    return $build;
  }

}
