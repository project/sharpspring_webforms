<?php

namespace Drupal\sharpspring_webforms_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller that contains callbacks for the webform handler.
 */
class WebformHandlerController extends ControllerBase {

  /**
   * Handle the test sharpspring call that is executed in the handler tests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   */
  public function handleCall(Request $request): Response {
    $this->state()->set('sharpspring_webforms.test.call_executed', TRUE);
    $this->state()->set('sharpspring_webforms.test.handler_data', $request->query->all());
    return new Response('');
  }

  /**
   * Handle the translated test call that is executed in the handler tests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   */
  public function handleTranslatedCall(Request $request): Response {
    $this->state()->set('sharpspring_webforms.test.translated_call_executed', TRUE);
    $this->state()->set('sharpspring_webforms.test.translated_handler_data', $request->query->all());
    return new Response('');
  }

  /**
   * Handle the test sharpspring call that is executed in the handler tests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   */
  public function handleExceptionCall(Request $request): Response {
    $this->state()->set('sharpspring_webforms.test.call_executed', TRUE);
    throw new AccessDeniedHttpException();
  }

}
