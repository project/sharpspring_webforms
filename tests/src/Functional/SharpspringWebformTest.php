<?php

namespace Drupal\Tests\sharpspring_webforms\Functional;

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform\Traits\WebformBrowserTestTrait;

/**
 * Tests the sharpspring webforms module.
 *
 * @group sharpspring_webforms
 */
class SharpspringWebformTest extends BrowserTestBase {

  use WebformBrowserTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sharpspring_webforms_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User with configure webform sharpsrping settings permission.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * A regular user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $webUser;

  /**
   * Id of webform 1.
   *
   * @var int|string|null
   */
  protected $webformId1;

  /**
   * Id of webform 2.
   *
   * @var int|string|null
   */
  protected $webformId2;

  /**
   * The webform ID used in the frontend for webform 1.
   *
   * @var string
   */
  protected $webformFormId1;

  /**
   * The webform ID used in the frontend for webform 2.
   *
   * @var string
   */
  protected $webformFormId2;

  /**
   * The webform storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $webformStorage;

  /**
   * The webform view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $webformViewBuilder;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->webUser = $this->createUser();
    $this->adminUser = $this->createUser([
      'administer webform',
      'configure webform sharpspring',
    ]);
    $this->webformId1 = $this->createWebform()->id();
    $this->webformId2 = $this->createWebform()->id();
    $this->webformFormId1 = Html::getId('webform-submission-' . $this->webformId1 . '-add-form');
    $this->webformFormId2 = Html::getId('webform-submission-' . $this->webformId2 . '-add-form');
    $this->webformStorage = $this->container->get('entity_type.manager')->getStorage('webform');
    $this->webformViewBuilder = $this->container->get('entity_type.manager')->getViewBuilder('webform');
  }

  /**
   * Tests the sharpspring webform config form.
   */
  public function testConfigForm() {
    // Check that by default no values are set.
    $webform = $this->webformStorage->loadUnchanged($this->webformId1);
    $this->assertEmpty($webform->getThirdPartySettings('sharpspring_webforms'));

    // Unauthorized user should not have access.
    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId1]));
    $this->assertSession()->statusCodeEquals(403);

    // Login as a regular user.
    $this->drupalLogin($this->webUser);

    // Unauthorized user should not have access.
    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId1]));
    $this->assertSession()->statusCodeEquals(403);

    // Login as an admin user.
    $this->drupalLogin($this->adminUser);

    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId1]));
    $this->assertSession()->statusCodeEquals(200);

    // Check if base uri and endpoint are required.
    $this->submitForm(['sharpspring_status' => TRUE], 'Save');
    $this->assertSession()->pageTextContains('Base URI field is required.');
    $this->assertSession()->pageTextContains('Endpoint field is required.');
    $this->assertSession()->pageTextNotContains('The configuration options have been saved.');

    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId1]));
    $this->submitForm([
      'sharpspring_status' => TRUE,
      'sharpspring_base_uri' => 'http://random-url.com',
    ], 'Save');
    $this->assertSession()->pageTextNotContains('Base URI field is required.');
    $this->assertSession()->pageTextContains('Endpoint field is required.');
    $this->assertSession()->pageTextNotContains('The configuration options have been saved.');

    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId1]));
    $this->submitForm([
      'sharpspring_status' => TRUE,
      'sharpspring_endpoint' => 'random-endpoint',
    ], 'Save');
    $this->assertSession()->pageTextContains('Base URI field is required.');
    $this->assertSession()->pageTextNotContains('Endpoint field is required.');
    $this->assertSession()->pageTextNotContains('The configuration options have been saved.');

    // Check if saving actually saves the configured values.
    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId1]));
    $this->submitForm([
      'sharpspring_status' => TRUE,
      'sharpspring_base_uri' => 'http://random-url.com/',
      'sharpspring_endpoint' => 'random-endpoint',
    ], 'Save');
    $this->assertSession()->pageTextNotContains('Base URI field is required.');
    $this->assertSession()->pageTextNotContains('Endpoint field is required.');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId1]));

    // Check that the values are actually saved.
    $webform = $this->webformStorage->loadUnchanged($this->webformId1);
    $this->assertEquals(TRUE, $webform->getThirdPartySetting('sharpspring_webforms', 'status'));
    $this->assertEquals('http://random-url.com/', $webform->getThirdPartySetting('sharpspring_webforms', 'base_uri'));
    $this->assertEquals('random-endpoint', $webform->getThirdPartySetting('sharpspring_webforms', 'endpoint'));

    // Make sure that a trailing slash is added to the base_uri.
    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId1]));
    $this->submitForm([
      'sharpspring_status' => TRUE,
      'sharpspring_base_uri' => 'http://random-url.com',
      'sharpspring_endpoint' => 'random-endpoint',
    ], 'Save');

    $webform = $this->webformStorage->loadUnchanged($this->webformId1);
    $this->assertEquals('http://random-url.com/', $webform->getThirdPartySetting('sharpspring_webforms', 'base_uri'));

    // Disabling sharpspring removes all third party settings.
    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId1]));
    $this->submitForm([
      'sharpspring_status' => FALSE,
      'sharpspring_base_uri' => '',
      'sharpspring_endpoint' => '',
    ], 'Save');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Check if removing of the values is working.
    $webform = $this->webformStorage->loadUnchanged($this->webformId1);
    $this->assertEmpty($webform->getThirdPartySettings('sharpspring_webforms'));
  }

  /**
   * Tests if the correct drupal settings are set.
   */
  public function testDrupalSettings() {
    // Check that the library and drupalSettings are not present when
    // sharpspring tracking is disabled.
    $webform = $this->webformStorage->loadUnchanged($this->webformId1);
    $this->drupalGet($webform->toUrl());
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayNotHasKey('sharpspring_webforms', $drupalSettings);
    $this->assertSession()->responseNotContains('sharpspring_webforms/assets/js/sharpspring_webforms.webform.js');

    $webform = $this->webformStorage->loadUnchanged($this->webformId1);
    $webform->setThirdPartySetting('sharpspring_webforms', 'status', TRUE);
    $webform->setThirdPartySetting('sharpspring_webforms', 'base_uri', 'http://random-url.com');
    $webform->setThirdPartySetting('sharpspring_webforms', 'endpoint', 'random-endpoint');
    $webform->save();

    // Check that the drupalSettings and library is included when the
    // sharpspring tracking is active.
    $this->drupalGet($webform->toUrl());
    $drupalSettings = $this->getDrupalSettings();
    $this->assertEquals('http://random-url.com', $drupalSettings['sharpspring_webforms'][$this->webformFormId1]['base_uri']);
    $this->assertEquals('random-endpoint', $drupalSettings['sharpspring_webforms'][$this->webformFormId1]['endpoint']);
    $this->assertEquals($this->webformFormId1, $drupalSettings['sharpspring_webforms'][$this->webformFormId1]['form_id']);
    $this->assertSession()->responseContains('sharpspring_webforms/assets/js/sharpspring_webforms.webform.js');
  }

  /**
   * Test sharpspring webforms with multiple webforms on the same page.
   */
  public function testDrupalSettingsMultipleWebforms() {
    $this->drupalGet(Url::fromRoute('sharpspring_webforms_test.webforms')->toString());
    $drupalSettings = $this->getDrupalSettings();
    $this->assertArrayNotHasKey('sharpspring_webforms', $drupalSettings);
    $this->assertSession()->responseNotContains('sharpspring_webforms/assets/js/sharpspring_webforms.webform.js');

    $webform = $this->webformStorage->loadUnchanged($this->webformId1);
    $webform->setThirdPartySetting('sharpspring_webforms', 'status', TRUE);
    $webform->setThirdPartySetting('sharpspring_webforms', 'base_uri', 'http://random-url.com');
    $webform->setThirdPartySetting('sharpspring_webforms', 'endpoint', 'random-endpoint');
    $webform->save();

    $this->drupalGet(Url::fromRoute('sharpspring_webforms_test.webforms')->toString());
    $drupalSettings = $this->getDrupalSettings();
    $this->assertEquals('http://random-url.com', $drupalSettings['sharpspring_webforms'][$this->webformFormId1]['base_uri']);
    $this->assertEquals('random-endpoint', $drupalSettings['sharpspring_webforms'][$this->webformFormId1]['endpoint']);
    $this->assertEquals($this->webformFormId1, $drupalSettings['sharpspring_webforms'][$this->webformFormId1]['form_id']);
    $this->assertArrayNotHasKey($this->webformFormId2, $drupalSettings['sharpspring_webforms']);
    $this->assertSession()->responseContains('sharpspring_webforms/assets/js/sharpspring_webforms.webform.js');

    $webform = $this->webformStorage->loadUnchanged($this->webformId2);
    $webform->setThirdPartySetting('sharpspring_webforms', 'status', TRUE);
    $webform->setThirdPartySetting('sharpspring_webforms', 'base_uri', 'http://random-url2.com');
    $webform->setThirdPartySetting('sharpspring_webforms', 'endpoint', 'random-endpoint2');
    $webform->save();

    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId1]));
    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId2]));

    $this->drupalGet(Url::fromRoute('sharpspring_webforms_test.webforms')->toString());
    $drupalSettings = $this->getDrupalSettings();
    $this->assertEquals('http://random-url.com', $drupalSettings['sharpspring_webforms'][$this->webformFormId1]['base_uri']);
    $this->assertEquals('random-endpoint', $drupalSettings['sharpspring_webforms'][$this->webformFormId1]['endpoint']);
    $this->assertEquals($this->webformFormId1, $drupalSettings['sharpspring_webforms'][$this->webformFormId1]['form_id']);
    $this->assertEquals('http://random-url2.com', $drupalSettings['sharpspring_webforms'][$this->webformFormId2]['base_uri']);
    $this->assertEquals('random-endpoint2', $drupalSettings['sharpspring_webforms'][$this->webformFormId2]['endpoint']);
    $this->assertEquals($this->webformFormId2, $drupalSettings['sharpspring_webforms'][$this->webformFormId2]['form_id']);
    $this->assertSession()->responseContains('sharpspring_webforms/assets/js/sharpspring_webforms.webform.js');
  }

}
