<?php

namespace Drupal\Tests\sharpspring_webforms\Functional;

use Drupal\Component\Utility\Html;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform\Traits\WebformBrowserTestTrait;

/**
 * Tests the SharpSpring webforms handler settings.
 *
 * @group sharpspring_webforms
 */
class SharpspringWebformsHandlerTest extends BrowserTestBase {

  use WebformBrowserTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'sharpspring_webforms_test',
    'webform_ui',
    'block',
    'dblog',
    'config_translation',
    'content_translation',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The ID of the webform.
   *
   * @var int|string|null
   */
  protected $webformId;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The webform ID used in the frontend.
   *
   * @var string
   */
  protected $webformFormId;

  /**
   * The webform storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $webformStorage;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->rootUser);

    // Create FR.
    ConfigurableLanguage::createFromLangcode('fr')->save();
    // Set prefixes to en and fr.
    $this->drupalGet('admin/config/regional/language/detection/url');
    $this->submitForm([
      'prefix[en]' => 'en',
      'prefix[fr]' => 'fr',
    ], 'Save configuration');
    // Set up URL and language selection page methods.
    $this->drupalGet('admin/config/regional/language/detection');
    $this->submitForm([
      'language_interface[enabled][language-url]' => 1,
    ], 'Save settings');

    // Create a webform with some default fields.
    $webform = $this->createWebform([], [
      'name' => [
        '#title' => 'Your name',
        '#type' => 'textfield',
        '#required' => TRUE,
      ],
      'email' => [
        '#title' => 'Your Email',
        '#type' => 'email',
        '#required' => TRUE,
      ],
      'address' => [
        '#title' => 'Address',
        '#type' => 'webform_address',
        '#required' => TRUE,
      ],
      'subject' => [
        '#title' => 'Subject',
        '#type' => 'checkboxes',
        '#required' => TRUE,
        '#options' => [
          'one' => 'Subject one',
          'two' => 'Subject two',
          'three' => 'Subject three',
          'four' => 'Subject four',
        ],
      ],
      'message' => [
        '#title' => 'Message',
        '#type' => 'textarea',
        '#required' => TRUE,
      ],
    ]);

    // Add the sharpspring webform handler.
    $handler = $this->container->get('plugin.manager.webform.handler')
      ->createInstance('sharpspring_remote_post', [
        'id' => 'sharpspring_remote_post',
        'label' => 'SharpSpring Remote Post',
        'handler_id' => 'sharpspring_remote_post',
        'status' => 1,
        'weight' => 0,
        'settings' => [
          'base_uri' => rtrim($this->baseUrl, '/') . '/sharpspring-webforms-test/handler/',
          'endpoint' => 'random_endpoint',
        ],
      ]);
    $webform->addWebformHandler($handler);
    $webform->save();
    $this->webformId = $webform->id();

    $this->webformFormId = Html::getId('webform-submission-' . $webform->id() . '-add-form');
    $this->webformStorage = $this->container->get('entity_type.manager')->getStorage('webform');
    $this->languageManager = $this->container->get('language_manager');
    $this->state = $this->container->get('state');
  }

  /**
   * Tests if adding a handler in the UI works correctly.
   */
  public function testAddingHandlerInUi() {
    $webform = $this->webformStorage->loadUnchanged($this->webformId);
    // Go to the handlers page and add a new sharpspring remote post handler.
    $this->drupalget($webform->toUrl('handlers'));
    $this->clickLink('Add handler');
    $this->clickLink('SharpSpring Remote Post');

    $this->submitForm([
      'label' => 'Test sharpspring handler',
      'handler_id' => 'test_sharpspring_handler',
      'settings[base_uri]' => 'http://example.org',
      'settings[endpoint]' => 'endpoint',
    ], 'Save');
    $this->assertSession()->pageTextContains('The webform handler was successfully added.');

    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->webformStorage->loadUnchanged($this->webformId);
    $handler = $webform->getHandler('test_sharpspring_handler');
    $configuration = $handler->getConfiguration();
    // Check that the settings are correctly added.
    $this->assertEquals('http://example.org', $configuration['settings']['base_uri']);
    $this->assertEquals('endpoint', $configuration['settings']['endpoint']);
  }

  /**
   * Test that the handler works and the data is correctly handled.
   */
  public function testHandler() {
    $webform = $this->webformStorage->loadUnchanged($this->webformId);
    $this->drupalget($webform->toUrl());

    $this->assertFalse($this->state->get('sharpspring_webforms.test.call_executed', FALSE));

    // Submit a webform.
    $this->submitForm([
      'name' => 'Name',
      'email' => 'mail@example.org',
      'address[address]' => 'address',
      'address[address_2]' => 'address 2',
      'address[city]' => 'address 2',
      'address[postal_code]' => '0000',
      'address[country]' => 'Belgium',
      'subject[one]' => TRUE,
      'subject[four]' => TRUE,
      'message' => 'Message',
    ], 'Submit');

    // Check that the submission was correctly executed.
    $this->assertSession()->responseContains(sprintf('New submission added to %s', $webform->label()));
    $this->assertTrue($this->state->get('sharpspring_webforms.test.call_executed', TRUE));
    // Check that the values are correctly sent to the configured URL.
    $this->assertEquals([
      'name' => 'Name',
      'email' => 'mail@example.org',
      'address__address' => 'address',
      'address__address_2' => 'address 2',
      'address__city' => 'address 2',
      'address__state_province' => '',
      'address__postal_code' => '0000',
      'address__country' => 'Belgium',
      'subject' => 'one,four',
      'message' => 'Message',
    ], $this->state->get('sharpspring_webforms.test.handler_data'));
  }

  /**
   * Test that submission still works when the executed call fails.
   */
  public function testExceptionInHandler() {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->webformStorage->loadUnchanged($this->webformId);
    $handler = $webform->getHandler('sharpspring_remote_post');
    $configuration = $handler->getConfiguration();
    // Update the base_uri to a URL that always gives a 403 error.
    $configuration['settings']['base_uri'] = rtrim($this->baseUrl, '/') . '/sharpspring-webforms-test/handler/exception/';
    $handler->setConfiguration($configuration);
    $webform->updateWebformHandler($handler);
    $webform->save();
    $this->drupalget($webform->toUrl());

    $this->assertFalse($this->state->get('sharpspring_webforms.test.call_executed', FALSE));

    $this->submitForm([
      'name' => 'Name',
      'email' => 'mail@example.org',
      'address[address]' => 'address',
      'address[address_2]' => 'address 2',
      'address[city]' => 'address 2',
      'address[postal_code]' => '0000',
      'address[country]' => 'Belgium',
      'subject[one]' => TRUE,
      'subject[four]' => TRUE,
      'message' => 'Message',
    ], 'Submit');

    $this->assertSession()->responseContains(sprintf('New submission added to %s', $webform->label()));
    $this->assertTrue($this->state->get('sharpspring_webforms.test.call_executed', TRUE));
    $this->drupalGet('admin/reports/dblog');
    // Check that an error message was logged.
    $this->assertSession()->responseContains('Call to SharpSpring failed. Reason:');
  }

  /**
   * Test that the handler is translatable.
   */
  public function testTranslatedHandler() {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->webformStorage->loadUnchanged($this->webformId);
    $this->drupalGet('admin/structure/webform/manage/' . $this->webformId . '/translate/fr/add');
    $this->submitForm([
      'translation[config_names][webform.webform.' . $this->webformId . '][handlers][sharpspring_remote_post][settings][base_uri]' => rtrim($this->baseUrl, '/') . '/sharpspring-webforms-test/handler-translated/',
      'translation[config_names][webform.webform.' . $this->webformId . '][handlers][sharpspring_remote_post][settings][endpoint]' => 'random_endpoint_fr',
    ], 'Save translation');

    $this->assertFalse($this->state->get('sharpspring_webforms.test.call_executed', FALSE));
    $this->drupalGet('en/webform/' . $this->webformId);
    $this->submitForm([
      'name' => 'Name',
      'email' => 'mail@example.org',
      'address[address]' => 'address',
      'address[address_2]' => 'address 2',
      'address[city]' => 'address 2',
      'address[postal_code]' => '0000',
      'address[country]' => 'Belgium',
      'subject[one]' => TRUE,
      'subject[four]' => TRUE,
      'message' => 'Message',
    ], 'Submit');

    // Check that the submission was correctly executed.
    $this->assertSession()->responseContains(sprintf('New submission added to %s', $webform->label()));
    $this->assertTrue($this->state->get('sharpspring_webforms.test.call_executed', TRUE));

    // Check that the values are correctly sent to the configured URL.
    $this->assertEquals([
      'name' => 'Name',
      'email' => 'mail@example.org',
      'address__address' => 'address',
      'address__address_2' => 'address 2',
      'address__city' => 'address 2',
      'address__state_province' => '',
      'address__postal_code' => '0000',
      'address__country' => 'Belgium',
      'subject' => 'one,four',
      'message' => 'Message',
    ], $this->state->get('sharpspring_webforms.test.handler_data'));

    $this->assertFalse($this->state->get('sharpspring_webforms.test.translated_call_executed', FALSE));
    $this->drupalGet('fr/webform/' . $this->webformId);
    $this->submitForm([
      'name' => 'Name',
      'email' => 'mail@example.org',
      'address[address]' => 'address',
      'address[address_2]' => 'address 2',
      'address[city]' => 'address 2',
      'address[postal_code]' => '0000',
      'address[country]' => 'Belgium',
      'subject[one]' => TRUE,
      'subject[four]' => TRUE,
      'message' => 'Message',
    ], 'Submit');

    // Check that the submission was correctly executed.
    $this->assertSession()->responseContains(sprintf('New submission added to %s', $webform->label()));
    $this->assertTrue($this->state->get('sharpspring_webforms.test.translated_call_executed', TRUE));
    // Check that the values are correctly sent to the configured URL.
    $this->assertEquals([
      'name' => 'Name',
      'email' => 'mail@example.org',
      'address__address' => 'address',
      'address__address_2' => 'address 2',
      'address__city' => 'address 2',
      'address__state_province' => '',
      'address__postal_code' => '0000',
      'address__country' => 'Belgium',
      'subject' => 'one,four',
      'message' => 'Message',
    ], $this->state->get('sharpspring_webforms.test.translated_handler_data'));
  }

}
