<?php

namespace Drupal\Tests\sharpspring_webforms\Functional;

use Drupal\Component\Utility\Html;
use Drupal\Core\Url;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\webform\Traits\WebformBrowserTestTrait;

/**
 * Tests translating the SharpSpring webforms settings.
 *
 * @group sharpspring_webforms
 */
class SharpspringWebformsTranslationTest extends BrowserTestBase {

  use WebformBrowserTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'sharpspring_webforms_test',
    'config_translation',
    'content_translation',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User with configure webform sharpsrping settings permission.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * Id of webform.
   *
   * @var int|string|null
   */
  protected $webformId;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The webform ID used in the frontend.
   *
   * @var string
   */
  protected $webformFormId;

  /**
   * The webform storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $webformStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin);

    // Create FR.
    ConfigurableLanguage::createFromLangcode('fr')->save();
    // Set prefixes to en and fr.
    $this->drupalGet('admin/config/regional/language/detection/url');
    $this->submitForm([
      'prefix[en]' => 'en',
      'prefix[fr]' => 'fr',
    ], 'Save configuration');
    // Set up URL and language selection page methods.
    $this->drupalGet('admin/config/regional/language/detection');
    $this->submitForm([
      'language_interface[enabled][language-url]' => 1,
    ], 'Save settings');

    $this->webformId = $this->createWebform()->id();
    // Create a webform with some default fields.
    $webform = $this->createWebform([], [
      'name' => [
        '#title' => 'Your name',
        '#type' => 'textfield',
        '#required' => TRUE,
      ],
    ]);
    $this->webformId = $webform->id();

    $this->webformFormId = Html::getId('webform-submission-' . $this->webformId . '-add-form');
    $this->webformStorage = $this->container->get('entity_type.manager')->getStorage('webform');
    $this->languageManager = $this->container->get('language_manager');
  }

  /**
   * Tests if the correct translated drupal settings are set.
   */
  public function testTranslatedDrupalSettings() {
    $this->drupalGet(Url::fromRoute('sharpspring_webforms.configuration', ['webform' => $this->webformId]));
    $this->submitForm([
      'sharpspring_status' => TRUE,
      'sharpspring_base_uri' => 'http://random-url.com/',
      'sharpspring_endpoint' => 'random-endpoint',
    ], 'Save');
    $this->drupalGet('admin/structure/webform/manage/' . $this->webformId . '/translate/fr/add');
    $this->submitForm([
      'translation[config_names][webform.webform.' . $this->webformId . '][third_party_settings][sharpspring_webforms][base_uri]' => 'http://random-url.com/fr',
      'translation[config_names][webform.webform.' . $this->webformId . '][third_party_settings][sharpspring_webforms][endpoint]' => 'random-endpoint-fr',
    ], 'Save translation');

    $this->drupalGet('en/webform/' . $this->webformId);
    $drupalSettings = $this->getDrupalSettings();
    $this->assertEquals('http://random-url.com/', $drupalSettings['sharpspring_webforms'][$this->webformFormId]['base_uri']);
    $this->assertEquals('random-endpoint', $drupalSettings['sharpspring_webforms'][$this->webformFormId]['endpoint']);

    $this->drupalGet('fr/webform/' . $this->webformId);
    $drupalSettings = $this->getDrupalSettings();
    $this->assertEquals('http://random-url.com/fr', $drupalSettings['sharpspring_webforms'][$this->webformFormId]['base_uri']);
    $this->assertEquals('random-endpoint-fr', $drupalSettings['sharpspring_webforms'][$this->webformFormId]['endpoint']);
  }

}
